﻿using System.Collections.Generic;
using System.Linq;
using ATM3.BLL.Services;
using ATM3.DAL.Entities;
using ATM3.DAL.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace ATM3.Tests
{
    [TestClass]
    public class AtmServiceTest
    {
        private Mock<IUnitOfWork> _efUnitOfWork;
        private AtmService _atmService;

        [TestInitialize]
        public void SetUp()
        {
            _efUnitOfWork = new Mock<IUnitOfWork>();

            _efUnitOfWork.Setup(x => x.Cards.GetCardId("1111000000000000")).Returns(1);
            _efUnitOfWork.Setup(x => x.Cards.Get(1)).Returns(new Card
            {
                Account = "1111000000000000",
                Id = 1,
                Admin = 0,
                Block = 0,
                Pin = 1111,
                Sum = 6000
            });

            _efUnitOfWork.Setup(x => x.Cards.Get(2)).Returns(new Card
            {
                Account = "2222000000000000",
                Id = 2,
                Admin = 0,
                Block = 3,
                Pin = 2222,
                Sum = 2000
            });

            _efUnitOfWork.Setup(x => x.Cards.Get(3)).Returns(new Card
            {
                Account = "3333000000000000",
                Id = 3,
                Admin = 1,
                Block = 0,
                Pin = 3333,
                Sum = 300
            });
            _efUnitOfWork.Setup(x => x.Cards.BlockIncrement(2)).Returns(4);
            _efUnitOfWork.Setup(x => x.Cards.IsAdmin(1)).Returns(false);
            _efUnitOfWork.Setup(x => x.Cards.IsAdmin(3)).Returns(true);
            _efUnitOfWork.Setup(x => x.Logs.GetSumToday(1)).Returns(1000);
            _efUnitOfWork.Setup(x => x.Logs.GetSumToday(2)).Returns(3500);
            _efUnitOfWork.Setup(x => x.Options.GetOptionDictionary()).Returns(new Dictionary
                <int, string>
                {
                    {1, "2000"},
                    {2, "4000"}
                }
            );

            _atmService = new AtmService(_efUnitOfWork.Object);
        }

        [TestMethod]
        public void GetCardId_CardExists_1returns()
        {
            var expected = 1;
            var actual = _atmService.GetCardId("1111000000000000");
            Assert.AreEqual(expected, actual, "The card doesn't exist");
        }

        [TestMethod]
        public void GetCardId_CardDoesNotExists_0returns()
        {
            var expected = 0;
            var actual = _atmService.GetCardId("2222000000000000");
            Assert.AreEqual(expected, actual, "The card does exist");
        }

        [TestMethod]
        public void CheckPin_PasswordMatch_0returns()
        {
            var expected = 0;
            var actual = _atmService.CheckPin(1, 1111);
            Assert.AreEqual(expected, actual, "The pin isn't correct");
        }

        [TestMethod]
        public void CheckPin_PasswordDoesNotMatch_2returns()
        {
            var expected = 2;
            var actual = _atmService.CheckPin(1, 2222);
            Assert.AreEqual(expected, actual, "The pin is correct");
        }

        [TestMethod]
        public void CheckPin_CardBlocked_1returns()
        {
            var expected = 1;
            var actual = _atmService.CheckPin(2, 3333);
            Assert.AreEqual(expected, actual, "The card isn't blocked");
        }

        [TestMethod]
        public void IsAdmin_DoesNotMatch_falseReturns()
        {
            const bool expected = false;
            bool actual = _atmService.IsAdmin(1);
            Assert.AreEqual(expected, actual, "The owner of card is admin");
        }

        [TestMethod]
        public void IsAdmin_Match_trueReturns()
        {
            var expected = true;
            var actual = _atmService.IsAdmin(3);
            Assert.AreEqual(expected, actual, "The owner of card isn't admin");
        }

        [TestMethod]
        public void WithdrawAble_Withdraw_0Returns()
        {
            var expected = 0;
            var actual = _atmService.WithdrawAble(1, 100);
            Assert.AreEqual(expected, actual, "The amount isn't able to withdraw");
        }

        [TestMethod]
        public void WithdrawAble_LowBalance_1Returns()
        {
            var expected = 1;
            var actual = _atmService.WithdrawAble(3, 400);
            Assert.AreEqual(expected, actual, "The amount is less than card's balance");
        }

        [TestMethod]
        public void WithdrawAble_SingleExcess_2Returns()
        {
            var expected = 2;
            var actual = _atmService.WithdrawAble(1, 3000);
            Assert.AreEqual(expected, actual, "The amount is less than single restriction ");
        }

        [TestMethod]
        public void WithdrawAble_DailyExcess_3Returns()
        {
            var expected = 3;
            var actual = _atmService.WithdrawAble(2, 1000);
            Assert.AreEqual(expected, actual, "The daily withdrawn amount is less than daily restriction");
        }

        [TestMethod]
        public void WithdrawAble_ZeroAmount_4Returns()
        {
            var expected = 4;
            var actual = _atmService.WithdrawAble(1, 0);
            Assert.AreEqual(expected, actual, "The amount is not equal to zero");
        }

        [TestMethod]
        public void GetBalance_Card1_6000returns()
        {
            var expected = 6000;
            var actual = _atmService.GetBalance(1);
            Assert.AreEqual(expected, actual, "The balance doesn't match");
        }

        [TestMethod]
        public void GetAccount_Id1_1111000000000000returns()
        {
            var expected = "1111000000000000";
            var actual = _atmService.GetAccount(1);
            Assert.AreEqual(expected, actual, "The account doesn't match");
        }

        [TestMethod]
        public void GetOptionDictionary_CountOfOptions_2returns()
        {
            // Act
            var expected = 2;
            var actual = _atmService.GetOptionDictionary().Count();

            // Assert
            Assert.AreEqual(expected, actual, "The option count is not correct");
        }
    }
}