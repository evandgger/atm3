﻿namespace ATM3.BLL.DTO
{
    public class OptionDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}