﻿using System.Collections.Generic;
using ATM3.BLL.DTO;

namespace ATM3.BLL.Interfaces
{
    public interface IAtmService
    {
        int GetCardId(string account);
        string GetAccount(int cardId);
        int CheckPin(int cardId, int pin);
        int WithdrawAble(int cardId, int amount);
        int GetBalance(int cardId, int priz);
        void AddLog(int cardId, int amount, int action);
        bool IsAdmin(int cardId);
        void Dispose();
        Dictionary<int, string> GetOptionDictionary();
        IEnumerable<OptionDto> GetAllOptions();
        void UpdateOptions(IEnumerable<OptionDto> option);
    }
}