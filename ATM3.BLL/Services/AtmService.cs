﻿using System;
using System.Collections.Generic;
using ATM3.BLL.DTO;
using ATM3.BLL.Interfaces;
using ATM3.DAL.Entities;
using ATM3.DAL.Interfaces;
using ATM3.DAL.Repositories;
using AutoMapper;

namespace ATM3.BLL.Services
{
    public class AtmService : IAtmService
    {
        private IUnitOfWork Db { get; }

        public AtmService(IUnitOfWork unitOfWork)
        {
            Db = unitOfWork;
        }

        public int GetCardId(string account)
        {
            return Db.Cards.GetCardId(account);
        }

        public int CheckPin(int cardId, int pin)
        {
            var doesMatch = Db.Cards.Get(cardId).Pin == pin;
            int result;

            if (doesMatch)
            {
                Db.Cards.ResetBlock(cardId);
                result = 0;
            }
            else
            {
                result = Db.Cards.BlockIncrement(cardId) >= CardRepository.AtmTries ? 1 : 2;
            }

            return result;
        }

        public bool IsAdmin(int cardId)
        {
            return Db.Cards.IsAdmin(cardId);
        }

        public int WithdrawAble(int cardId, int amount)
        {
            int result;
            var card = Db.Cards.Get(cardId);
            var options = Db.Options.GetOptionDictionary();
            var once = Convert.ToInt32(options[1]);
            var perday = Convert.ToInt32(options[2]);
            var sumToday = Db.Logs.GetSumToday(cardId);

            if (amount == 0)
            {
                result = 4;
            }
            else if (amount > once)
            {
                result = 2;
            }
            else if (card.Sum < amount)
            {
                result = 1;
            }
            else if (amount + sumToday > perday)
            {
                result = 3;
            }
            else
            {
                Db.Cards.Withdraw(cardId, amount);
                Db.Logs.Add(cardId, amount, 1);
                result = 0;
            }

            return result;
        }

        public int GetBalance(int cardId, int priz = 0)
        {
            if (priz == 0) Db.Logs.Add(cardId);
            return Db.Cards.Get(cardId).Sum;
        }

        public string GetAccount(int cardId)
        {
            return Db.Cards.Get(cardId).Account;
        }

        public void AddLog(int cardId, int amount, int action)
        {
            Db.Logs.Add(cardId, amount, action);
        }

        public void Dispose()
        {
            Db.Dispose();
        }

        public Dictionary<int, string> GetOptionDictionary()
        {
            return Db.Options.GetOptionDictionary();
        }

        public IEnumerable<OptionDto> GetAllOptions()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<Option, OptionDto>()).CreateMapper();
            return mapper.Map<IEnumerable<Option>, List<OptionDto>>(Db.Options.GetAll());
        }

        public void UpdateOptions(IEnumerable<OptionDto> optionDto)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OptionDto, Option>()).CreateMapper();
            Db.Options.UpdateOptions(mapper.Map<IEnumerable<OptionDto>, List<Option>>(optionDto));
        }
    }
}