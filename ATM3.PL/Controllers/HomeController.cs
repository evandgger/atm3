﻿using System;
using System.Web.Mvc;
using ATM3.BLL.Interfaces;
using ATM3.PL.Helpers;

namespace ATM3.PL.Controllers
{
    public class HomeController : Controller
    {
        private enum Msg
        {
            CardAbsent = 1,
            CardBlocked,
            PinError,
            SumZero,
            SumBig,
            SumOnce,
            SumPerday
        }

        private readonly IAtmService _atmService;

        public HomeController(IAtmService serv)
        {
            _atmService = serv;
        }

        public ActionResult Error404()
        {
            return RedirectToAction("Index");
        }

        public ActionResult Index()
        {
            Session.Clear();
            return View();
        }

        public ActionResult PinCode()
        {
            ActionResult result;
            if (HasSession("id")) result = View();
            else result = RedirectToAction("Index");
            return result;
        }

        public ActionResult Menu()
        {
            ActionResult result;
            if (HasSession("id") && HasSession("pin")) result = View();
            else result = RedirectToAction("Index");
            return result;
        }

        public ActionResult Balance()
        {
            ActionResult result;
            if (HasSession("id") && HasSession("pin"))
            {
                var cardId = (int) Session["id"];
                ViewBag.sum = _atmService.GetBalance(cardId, 0);
                ViewBag.account = Lib.FormatCard(_atmService.GetAccount(cardId));
                ViewBag.dat = FormatDate(DateTime.Now);
                result = View();
            }
            else
            {
                result = RedirectToAction("Index");
            }

            return result;
        }

        public ActionResult Withdraw()
        {
            ActionResult result;
            if (HasSession("id") && HasSession("pin")) result = View();
            else result = RedirectToAction("Index");
            return result;
        }

        public ActionResult Report()
        {
            ActionResult result;
            if (HasSession("id") && HasSession("pin"))
            {
                var cardId = (int) Session["id"];
                ViewBag.rest = _atmService.GetBalance(cardId, 1);
                ViewBag.sum = Session["sum"];
                Session["sum"] = null;
                ViewBag.account = Lib.FormatCard(_atmService.GetAccount(cardId));
                ViewBag.dat = FormatDate(DateTime.Now);
                result = View();
            }
            else
            {
                result = RedirectToAction("Index");
            }

            return result;
        }

        public ActionResult MessageError()
        {
            if (HasSession("msg"))
            {
                ViewBag.msg = Session["msg"];
                Session["msg"] = null;
                return View();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public int CheckAccount(string account)
        {
            var cardId = _atmService.GetCardId(account);
            if (cardId == -1) Session["msg"] = (int) Msg.CardAbsent; // If card doesn't exist
            else if (cardId == 0) Session["msg"] = (int) Msg.CardBlocked; // If card is blocked
            else Session["id"] = cardId; // If card exists and isn't blocked
            return HasSession("msg") ? 0 : 1;
        }

        [HttpPost]
        public int CheckPin(string pin)
        {
            int cardId = (int) Session["id"], ret = 1;
            var result = _atmService.CheckPin(cardId, Convert.ToInt32(pin));
            if (result == 0)
            {
                Session["pin"] = true;
                if (_atmService.IsAdmin(cardId)) ret = -1; // Admin !!!
            }
            else if (result == 1)
            {
                Session["msg"] = (int) Msg.CardBlocked; // If tries of entering > ATM_TRY -1
            }
            else if (result == 2)
            {
                Session["msg"] = (int) Msg.PinError; // If pin-code doesn't match
            }

            return HasSession("msg") ? 0 : ret;
        }

        [HttpPost]
        public int CheckSum(int sum)
        {
            sum = Math.Max(0, sum);
            var cardId = (int) Session["id"];
            var result = _atmService.WithdrawAble(cardId, sum);
            if (result == 4) Session["msg"] = (int) Msg.SumZero;
            else if (result == 1) Session["msg"] = (int) Msg.SumBig;
            else if (result == 2) Session["msg"] = (int) Msg.SumOnce;
            else if (result == 3) Session["msg"] = (int) Msg.SumPerday;
            else Session["sum"] = sum;
            return HasSession("msg") ? 0 : 1;
        }

        private bool HasSession(string session) // Does exist the session
        {
            return Session[session] != null;
        }

        private string FormatDate(DateTime dat, int count = 16) // Date without seconds
        {
            return dat.ToString("dd.MM.yyyy HH:mm");
        }
    }
}