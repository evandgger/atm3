﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using ATM3.BLL.DTO;
using ATM3.BLL.Interfaces;
using ATM3.PL.Models;
using AutoMapper;

namespace ATM3.PL.Controllers
{
    public class AdminController : Controller
    {
        private readonly IAtmService _atmService;

        public AdminController(IAtmService serv)
        {
            _atmService = serv;
        }

        public ActionResult Index()
        {
            if (HasSession("id") && HasSession("pin")) return View();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Config()
        {
            if (HasSession("id") && HasSession("pin"))
            {
                var options = _atmService.GetAllOptions();
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OptionDto, OptionViewModel>()).CreateMapper();
                return View(mapper.Map<IEnumerable<OptionDto>, List<OptionViewModel>>(options));
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public int SaveOptions(IEnumerable<OptionViewModel> fields)
        {
            var result = 0;
            if (IsCorrectOptions(fields) != 0)
            {
                var mapper = new MapperConfiguration(cfg => cfg.CreateMap<OptionViewModel, OptionDto>()).CreateMapper();
                _atmService.UpdateOptions(mapper.Map<IEnumerable<OptionViewModel>, List<OptionDto>>(fields));
                result = 1;
            }

            return result;
        }

        private bool HasSession(string session) // Does exist the session
        {
            return Session[session] != null;
        }

        private int IsCorrectOptions(IEnumerable<OptionViewModel> fields)
        {
            var result = 1;
            foreach (var field in fields)
            {
                var regex = new Regex(@"[\d]");
                if (!regex.IsMatch(field.Value))
                {
                    result = 0;
                    break;
                }
            }

            return result;
        }
    }
}