﻿namespace ATM3.PL.Helpers

{
    public class Lib
    {
        public static string FormatCard(string account) // Formated card-number
        {
            return account.Substring(0, 4) + "-"
                                           + account.Substring(4, 4) + "-"
                                           + account.Substring(8, 4) + "-"
                                           + account.Substring(12, 4);
        }
    }
}