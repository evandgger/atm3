﻿using System.Web.Mvc;

namespace ATM3.PL.Helpers
{
    public static class AtmHelper
    {
        public static MvcHtmlString Menu(string pageId, string[,] buttons)
        {
            var div = new TagBuilder("div");
            div.GenerateId(pageId);
            for (var i = 0; i < buttons.GetLength(0); i++)
            {
                var btn = new TagBuilder("button");
                btn.AddCssClass(buttons[i, 0]);
                btn.MergeAttribute("onclick", "window.location.href = '/" + buttons[i, 1] + "/'");
                btn.SetInnerText(buttons[i, 2]);
                div.InnerHtml += btn.ToString();
            }

            return new MvcHtmlString(div.ToString());
        }

        public static MvcHtmlString ViewBalance(string acc, string dat, string sum)
        {
            var account = new TagBuilder("input");
            account.GenerateId("account");
            account.MergeAttribute("placeholder", acc);
            account.MergeAttribute("unselectable", "on");
            account.MergeAttribute("readonly", "on");
            var div = new TagBuilder("div");
            div.AddCssClass("info");

            var pDat = new TagBuilder("p");
            pDat.AddCssClass("current_dat");
            pDat.SetInnerText("Текущая дата:");
            div.InnerHtml += pDat.ToString();
            var h3Dat = new TagBuilder("h3");
            h3Dat.SetInnerText(dat);
            div.InnerHtml += h3Dat.ToString();

            var h3Sum = new TagBuilder("h3");
            var spanSum = new TagBuilder("span");
            spanSum.AddCssClass("sum");
            spanSum.InnerHtml = sum;
            h3Sum.InnerHtml = "Баланс на карте: " + spanSum;
            div.InnerHtml += h3Sum.ToString();
            return new MvcHtmlString(account + div.ToString());
        }

        public static MvcHtmlString ViewReport(string acc, string dat, string sum, string rest)
        {
            var account = new TagBuilder("input");
            account.GenerateId("account");
            account.MergeAttribute("placeholder", acc);
            account.MergeAttribute("unselectable", "on");
            account.MergeAttribute("readonly", "on");
            var div = new TagBuilder("div");
            div.AddCssClass("info");

            var pDat = new TagBuilder("p");
            pDat.AddCssClass("current_dat");
            pDat.SetInnerText("Текущая дата:");
            div.InnerHtml += pDat.ToString();
            pDat = new TagBuilder("p");
            pDat.SetInnerText(dat);
            pDat.AddCssClass("current_dat");
            div.InnerHtml += pDat.ToString();

            var h4Sum = new TagBuilder("h4");
            var spanSum = new TagBuilder("span");
            spanSum.AddCssClass("rest");
            spanSum.InnerHtml = sum;
            h4Sum.InnerHtml = "Снято: " + spanSum;
            div.InnerHtml += h4Sum.ToString();

            h4Sum = new TagBuilder("h4");
            spanSum = new TagBuilder("span");
            spanSum.AddCssClass("sum");
            spanSum.InnerHtml = rest;
            h4Sum.InnerHtml = "Остаток: " + spanSum;

            div.InnerHtml += h4Sum.ToString();
            return new MvcHtmlString(account + div.ToString());
        }

        public static MvcHtmlString Numpad(string idInput, string placeholder, string idPage)
        {
            var account = new TagBuilder("input");
            account.GenerateId(idInput);
            account.MergeAttribute("unselectable", "on");
            account.MergeAttribute("readonly", "on");
            account.MergeAttribute("placeholder", placeholder);

            var hide = new TagBuilder("span");
            if (idInput == "pincode")
            {
                hide.GenerateId("pincode_hide");
                hide.MergeAttribute("style", "display:none");
            }

            var left = new TagBuilder("div");
            left.AddCssClass("left");
            char[] numbs = {'1', '2', '3', '4', '5', '6', '7', '8', '9', '<', '0', '.'};
            var i = 0;

            var btnGroup = new TagBuilder("div");
            btnGroup.AddCssClass("btn1-group");
            foreach (var numb in numbs)
            {
                i++;
                if (i > 1 && i % 3 == 1)
                {
                    left.InnerHtml += btnGroup.ToString();
                    btnGroup = new TagBuilder("div");
                    btnGroup.AddCssClass("btn1-group");
                }

                var btn1 = new TagBuilder("button");
                btn1.AddCssClass("btn1");
                btn1.SetInnerText(numb.ToString());
                if (i == 12) btn1.MergeAttribute("disabled", "on");
                btnGroup.InnerHtml += btn1.ToString();
            }

            left.InnerHtml += btnGroup.ToString();

            var div = new TagBuilder("div");
            div.GenerateId(idPage);
            div.AddCssClass("numpad");
            div.InnerHtml += left.ToString();

            var right = new TagBuilder("div");
            right.AddCssClass("right clearfix");

            btnGroup = new TagBuilder("div");
            btnGroup.AddCssClass("btn1-group");
            var btn = new TagBuilder("button");
            btn.AddCssClass("btn1 clear");
            btn.SetInnerText("Очистить");
            btnGroup.InnerHtml += btn.ToString();
            btn = new TagBuilder("button");
            btn.AddCssClass("btn1 submit");
            btn.SetInnerText("OK");
            btnGroup.InnerHtml += btn.ToString();
            if (idInput != "account")
            {
                btn = new TagBuilder("button");
                btn.AddCssClass("btn1 cancel");
                btn.SetInnerText("Выход");
                btn.MergeAttribute("onclick", "window.location.href = '/Home/Index/'");
                btnGroup.InnerHtml += btn.ToString();
            }

            right.InnerHtml += btnGroup.ToString();
            div.InnerHtml += right.ToString();
            return new MvcHtmlString(account + hide.ToString() + div);
        }

        public static MvcHtmlString ViewMessage(int msg)
        {
            string[,] messages =
            {
                {"Введенная карта не существует", "Index"}, // 1
                {"Введенная карта заблокирована", "Index"}, // 2
                {"Введенный пин-код неверен", "PinCode"}, // 3
                {"Вы не ввели сумму", "Withdraw"}, // 4
                {"Введенная сумма превышает остаток", "Balance"}, // 5
                {"Превышение разового ограничения", "Withdraw"}, // 6
                {"Превышение суточного ограничения", "Withdraw"} // 7
            };

            var div = new TagBuilder("div");
            var h3Msg = new TagBuilder("h3");
            h3Msg.SetInnerText(messages[msg, 0]);
            div.InnerHtml += h3Msg.ToString();

            var btn = new TagBuilder("button");
            btn.AddCssClass("button_back");
            btn.SetInnerText("Назад");
            btn.MergeAttribute("onclick", "window.location.href = '/Home/" + messages[msg, 1] + "/'");
            div.InnerHtml += btn.ToString();

            return new MvcHtmlString(div.ToString());
        }
    }
}