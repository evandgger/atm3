﻿using System.Collections.Generic;
using System.Web.Mvc;
using ATM3.PL.Models;

namespace ATM3.PL.Helpers
{
    public class AdminHelper
    {
        public static MvcHtmlString ConfigEdit(string title, string idPage, IEnumerable<OptionViewModel> model)
        {
            var h2 = new TagBuilder("h2");
            h2.SetInnerText(title);

            var tr = new TagBuilder("tr"); // Header of table
            var td = new TagBuilder("th");
            td.SetInnerText("Наименование");
            tr.InnerHtml += td.ToString();
            td = new TagBuilder("th");
            td.SetInnerText("Значение");
            tr.InnerHtml += td.ToString();
            var table = new TagBuilder("table");
            table.GenerateId("config");
            table.AddCssClass("tabedit");
            table.MergeAttribute("border", "1");
            table.MergeAttribute("cellspacing", "0");
            table.InnerHtml += tr.ToString();

            foreach (var opt in model) // Editable rows 
            {
                tr = new TagBuilder("tr");
                td = new TagBuilder("td");
                td.SetInnerText(opt.Name);
                tr.InnerHtml += td.ToString();
                td = new TagBuilder("td");
                var input = new TagBuilder("input");
                input.MergeAttribute("type", "text");
                input.AddCssClass("vals");
                input.MergeAttribute("name", opt.Id.ToString());
                input.MergeAttribute("data-old", opt.Value);
                input.MergeAttribute("value", opt.Value);
                td.InnerHtml += input.ToString();
                tr.InnerHtml += td.ToString();
                table.InnerHtml += tr.ToString();
            }

            var div = new TagBuilder("div"); //  div with control buttons
            div.GenerateId(idPage);
            div.AddCssClass("btn2-group");
            var button = new TagBuilder("button");
            button.AddCssClass("btn2 submit");
            button.SetInnerText("Изменить");
            div.InnerHtml += button.ToString();
            button = new TagBuilder("button");
            button.AddCssClass("btn2 clear");
            button.SetInnerText("Отменить");
            div.InnerHtml += button.ToString();
            button = new TagBuilder("button");
            button.AddCssClass("btn2 exit");
            button.SetInnerText("Выход");
            button.MergeAttribute("onclick", "window.location.href = '/Admin/Index/'");
            div.InnerHtml += button.ToString();

            return new MvcHtmlString(h2 + table.ToString() + div);
        }
    }
}