﻿using ATM3.BLL.Interfaces;
using ATM3.BLL.Services;
using Ninject.Modules;

namespace ATM3.PL.Util
{
    public class AtmModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IAtmService>().To<AtmService>();
        }
    }
}