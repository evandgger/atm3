﻿namespace ATM3.PL.Models
{
    public class OptionViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}