﻿using System;

namespace ATM3.PL.Models
{
    public class LogViewModel
    {
        public int Id { get; set; }
        public DateTime Dat { get; set; }
        public int Sum { get; set; }
        public int Op { get; set; }
        public int CardId { get; set; }
    }
}