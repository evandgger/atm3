﻿using Ninject;
using Ninject.Modules;
using Ninject.Web.Mvc;
using ATM3.BLL.Infrastructure;
using ATM3.PL.Util;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ATM3.PL
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            NinjectModule atmModule = new AtmModule();
            NinjectModule serviceModule = new ServiceModule("DefaultConnection");
            var kernel = new StandardKernel(atmModule, serviceModule);
            DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
        }
    }
}