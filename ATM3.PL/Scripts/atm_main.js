﻿$(document).ready(function() {

	$("#account, #pincode").val("");
	$("#pincode_hide").text("");
	$(".right .submit").prop("disabled", true);

// Page-1: Account
	$("#Page1 .left .btn1").on("click",
		function(e) {
			var btn = $(this).text(), text = $("#account").val();
			var len = text.length;
			if (btn >= 0 && btn <= 9 && len < 19) {
				var defis = (len === 4 || len === 9 || len === 14) ? "-" : "";
				text += (defis + btn);
				$("#account").val(text);
			}
			if (btn === "<") {
				text = text.slice(0, -1);
				if (text.length % 5 === 0) text = text.slice(0, -1);
				$("#account").val(text);
			}

			len = $("#account").val().length;
			$(".right .submit").prop("disabled", (len !== 19));
		});

	function clearAccount() {
		$("#account").val("");
		$(".right .submit").prop("disabled", true);
	}

	$("#Page1 .right .clear").on("click", clearAccount);

	$("#Page1 .right .submit").on("click",
		function(e) {
			var card = ($("#account").val()).replace(/[-]/g, "");
			AntiForgeryToken();
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "/Home/CheckAccount/",
				beforeSend: loaderShow,
				data: { account: card },
				success: function(msg) {
					loaderHide();
					if (msg) window.location.href = "/Home/PinCode/";
					else {
						window.location.href = "/Home/MessageError/";
					}
				}
			});
		});


// Page-2: PinCode
	$("#Page2 .left .btn1").on("click",
		function(e) {
			var btn = $(this).text(), text = $("#pincode").val(), hide = $("#pincode_hide").text();
			var len = text.length;
			if (btn >= 0 && btn <= 9 && len < 4) {
				hide += btn;
				text += "*";
				$("#pincode").val(text);
				$("#pincode_hide").text(hide);
			}
			if (btn === "<") {
				text = text.slice(0, -1);
				hide = hide.slice(0, -1);
				$("#pincode").val(text);
				$("#pincode_hide").text(hide);
			}

			len = $("#pincode").val().length;
			$(".right .submit").prop("disabled", (len !== 4));
		});

	function clearPin() {
		$("#pincode").val("");
		$("#pincode_hide").text("");
		$(".right .submit").prop("disabled", true);
	}

	$("#Page2 .right .clear").on("click", clearPin);

	$("#Page2 .right .submit").on("click",
		function(e) {
			var pin = $("#pincode_hide").text();
			AntiForgeryToken();
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "/Home/CheckPin/",
				data: { pin: pin },
				beforeSend: loaderShow,
				success: function(msg) {
					loaderHide();
					if (msg === -1) window.location.href = "/Admin/Index/";
					else if (msg > 0) window.location.href = "/Home/Menu/";
					else {
						window.location.href = "/Home/MessageError/";
					}
				}
			});

		});


// Page-5: Withdraw
	$("#Page5 .left .btn1").on("click",
		function(e) {
			var btn = $(this).text(), text = $("#withdraw").val();
			var len = text.length;
			if (btn >= 0 && btn <= 9 && len <= 5) {
				text += btn;
				$("#withdraw").val(text);
			}
			if (btn === "<") {
				text = text.slice(0, -1);
				$("#withdraw").val(text);
			}

			len = $("#withdraw").val().length;
			$(".right .submit").prop("disabled", (len === 0));

		});

	$("#Page5 .right .clear").on("click",
		function(e) {
			$("#withdraw").val("");
		});

	$("#Page5 .right .submit").on("click",
		function(e) {
			var str = $("#withdraw").val();
			var check = (str.match(/^\d{1,5}$/) == null);
			var sum = (str.length === 0 || check) ? 0 : parseInt(str);
			AntiForgeryToken();
			$.ajax({
				type: "POST",
				dataType: "json",
				url: "/Home/CheckSum/",
				data: { sum: sum },
				beforeSend: loaderShow,
				success: function(msg) {
					loaderHide();
					if (msg) window.location.href = "/Home/Report/";
					else {
						window.location.href = "/Home/MessageError/";
					}
				}
			});
		});


// Page-9: Admin-Config
	$("#config .vals").on("keypress",
		function(e) { // Keypress: INT
			e = e || event;
			if (e.ctrlKey || e.altKey || e.metaKey) return false;
			var chr = getChar(e);
			if (chr == null) return false;
			if (chr < "0" || chr > "9") return false;
		});

	$("#Page9 .submit").on("click",
		function(e) {
			var fields = [];
			$("#config").find("input").each(function(index, element) {
				var el = $(element);
				if (el.prop("value") !== el.attr("data-old")) {
					var name = $(el.parent().parent().find("td")[0]).text();
					fields.push({ Id: parseInt(el.prop("name")), Name: name, Value: el.val() });
				}
			});

			if (fields.length) {
				AntiForgeryToken();
				$.ajax({
					type: "POST",
					dataType: "json",
					url: "/Admin/SaveOptions/",
					data: { fields: fields },
					beforeSend: loaderShow,
					success: function(ret) {
						loaderHide();
						if (ret === 0) clearOptions();
					}
				});
			}
		});

	$("#Page9 .clear").on("click", clearOptions);

});

function loaderShow() { $(".loader").css("display", "block"); }

function loaderHide() { $(".loader").css("display", "none"); }

function getChar(event) {
	if (event.which == null) { // IE
		if (event.keyCode < 32) return null; // спец. символ
		return String.fromCharCode(event.keyCode);
	}
	if (event.which !== 0 && event.charCode !== 0) { // все кроме IE
		if (event.which < 32) return null; // спец. символ
		return String.fromCharCode(event.which); // остальные
	}
	return null; // спец. символ
}

function clearOptions() {
	$("#config").find("input").each(function(index, element) {
		var el = $(element);
		if (el.prop("value") !== el.attr("data-old")) el.val(el.attr("data-old"));
	});
}

function AntiForgeryToken() {
	$.ajaxPrefilter(function(options, originalOptions, jqXhr) { // AntiForgeryToken
		if (originalOptions.type === "POST") {
			if ($('input[type="hidden"][name="__RequestVerificationToken"]').length !== 1) {
//				console.log(options);			// выводим в консоль объект,содержащий параметры текущего запроса
//				console.log(originalOptions);	// выводим в консоль объект,содержащий параметры запроса $.ajax()
//				jqXhr.abort();					// прерываем AJAX запрос

				window.location.href = "/Home/Index/";
			}
		}
	});
}