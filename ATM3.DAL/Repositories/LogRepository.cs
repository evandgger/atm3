﻿using System;
using System.Data.Entity;
using System.Linq;
using ATM3.DAL.EF;
using ATM3.DAL.Entities;
using ATM3.DAL.Interfaces;

namespace ATM3.DAL.Repositories
{
    public class LogRepository : ILogRepository
    {
        private enum Op
        {
            Balance,
            Withdraw
        }

        private readonly Atm3Context _db;

        public LogRepository(Atm3Context context)
        {
            _db = context;
        }

        public Log Get(int id)
        {
            return _db.Logs.Find(id);
        }

        public void Update(Log log)
        {
            _db.Entry(log).State = EntityState.Modified;
        }

        public void Add(int cardId, int sum = 0, int op = (int) Op.Balance)
        {
            var log = new Log
            {
                Dat = DateTime.Now,
                Card_Id = cardId,
                Sum = sum,
                Op = op
            };
            _db.Logs.Add(log);
            Save();
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        private bool _disposed;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing) _db.Dispose();

                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public int GetSumToday(int cardId)
        {
            var dat = DateTime.Now;
            var start = DateTime.Today;
            var logs = _db.Logs.Where(x => x.Card_Id == cardId
                                           && x.Op == (int) Op.Withdraw
                                           && x.Dat > start && x.Dat < dat
            ).ToList();
            return logs.Select(x => x.Sum).Sum();
        }
    }
}