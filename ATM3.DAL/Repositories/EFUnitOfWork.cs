﻿using System;
using ATM3.DAL.EF;
using ATM3.DAL.Interfaces;

namespace ATM3.DAL.Repositories
{
    public class EfUnitOfWork : IUnitOfWork
    {
        private readonly Atm3Context _db;
        private CardRepository _cardRepository;
        private LogRepository _logRepository;
        private OptionRepository _optionRepository;

        public EfUnitOfWork(string connectionString)
        {
            _db = new Atm3Context(connectionString);
        }

        public ICardRepository Cards
        {
            get
            {
                if (_cardRepository == null)
                    _cardRepository = new CardRepository(_db);
                return _cardRepository;
            }
        }

        public ILogRepository Logs
        {
            get
            {
                if (_logRepository == null)
                    _logRepository = new LogRepository(_db);
                return _logRepository;
            }
        }

        public IOptionRepository Options
        {
            get
            {
                if (_optionRepository == null)
                    _optionRepository = new OptionRepository(_db);
                return _optionRepository;
            }
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        private bool _disposed;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing) _db.Dispose();

                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}