﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using ATM3.DAL.EF;
using ATM3.DAL.Entities;
using ATM3.DAL.Interfaces;

namespace ATM3.DAL.Repositories
{
    public class OptionRepository : IOptionRepository
    {
        private readonly Atm3Context _db;

        public OptionRepository(Atm3Context context)
        {
            _db = context;
        }

        public IEnumerable<Option> GetAll()
        {
            return _db.Options;
        }

        public void Update(Option option)
        {
            _db.Entry(option).State = EntityState.Modified;
        }

        public Dictionary<int, string> GetOptionDictionary()
        {
            var dic = new Dictionary<int, string>();
            var opts = _db.Options;
            foreach (var opt in opts) dic.Add(opt.Id, opt.Value);
            return dic;
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        private bool _disposed;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing) _db.Dispose();

                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void UpdateOptions(List<Option> options)
        {
            foreach (var option in options) Update(option);
            Save();
        }
    }
}