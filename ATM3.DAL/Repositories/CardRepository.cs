﻿using System;
using System.Data.Entity;
using System.Linq;
using ATM3.DAL.EF;
using ATM3.DAL.Entities;
using ATM3.DAL.Interfaces;

namespace ATM3.DAL.Repositories
{
    public class CardRepository : ICardRepository
    {
        public const int AtmTries = 4;
        private readonly Atm3Context _db;

        public CardRepository(Atm3Context context)
        {
            _db = context;
        }

        public bool IsAdmin(int id)
        {
            bool result;
            var card = _db.Cards.Find(id);
            if (card == null) result = false;
            else result = card.Admin > 0;
            return result;
        }

        public Card Get(int id)
        {
            return _db.Cards.Find(id);
        }

        public int BlockIncrement(int id)
        {
            var result = -1;
            var card = _db.Cards.Find(id);
            if (card != null)
            {
                card.Block++;
                Save();
                result = card.Block;
            }

            return result;
        }

        public void Withdraw(int id, int amount)
        {
            var card = _db.Cards.Find(id);
            if (card != null)
            {
                card.Sum -= amount;
                Save();
            }
        }

        public void ResetBlock(int id)
        {
            var card = _db.Cards.Find(id);
            if (card != null)
            {
                card.Block = 0;
                Save();
            }
        }

        public void Update(Card card)
        {
            _db.Entry(card).State = EntityState.Modified;
        }

        public int GetCardId(string account)
        {
            int result;
            var card = _db.Cards.FirstOrDefault(x => x.Account == account);
            if (card == null) result = -1; // If card doesn't exist
            else if (card.Block >= AtmTries) result = 0; // If card is blocked
            else result = card.Id; // If card exists and isn't blocked
            return result;
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        private bool _disposed;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing) _db.Dispose();

                _disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}