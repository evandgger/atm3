﻿using System;
using ATM3.DAL.Entities;

namespace ATM3.DAL.Interfaces
{
    public interface ILogRepository : IDisposable
    {
        void Add(int cardId, int sum = 0, int op = 0);
        Log Get(int id);
        void Update(Log log);
        int GetSumToday(int cardId);
    }
}