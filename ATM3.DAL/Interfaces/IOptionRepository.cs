﻿using System;
using System.Collections.Generic;
using ATM3.DAL.Entities;

namespace ATM3.DAL.Interfaces
{
    public interface IOptionRepository : IDisposable
    {
        IEnumerable<Option> GetAll();
        void Update(Option option);
        Dictionary<int, string> GetOptionDictionary();
        void UpdateOptions(List<Option> options);
    }
}