﻿using System;

namespace ATM3.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ICardRepository Cards { get; }
        ILogRepository Logs { get; }
        IOptionRepository Options { get; }
        void Save();
    }
}