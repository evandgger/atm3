﻿using System;
using ATM3.DAL.Entities;

namespace ATM3.DAL.Interfaces
{
    public interface ICardRepository : IDisposable
    {
        int GetCardId(string account);
        Card Get(int id);
        void Update(Card card);
        int BlockIncrement(int id);
        void ResetBlock(int id);
        bool IsAdmin(int id);
        void Withdraw(int id, int amount);
        void Save();
    }
}