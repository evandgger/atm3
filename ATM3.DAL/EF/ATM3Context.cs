﻿using System.Data.Entity;
using ATM3.DAL.Entities;

namespace ATM3.DAL.EF
{
    public class Atm3Context : DbContext
    {
        public DbSet<Card> Cards { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<Option> Options { get; set; }

        public Atm3Context(string connectionString) : base("ATM3Context")
        {

        }
    }
}