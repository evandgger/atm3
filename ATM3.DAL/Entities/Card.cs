﻿namespace ATM3.DAL.Entities
{
    public class Card
    {
        public int Id { get; set; }
        public string Account { get; set; }
        public int Pin { get; set; }
        public int Sum { get; set; }
        public int Block { get; set; }
        public int Admin { get; set; }
    }
}