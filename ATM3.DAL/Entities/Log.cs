﻿using System;

namespace ATM3.DAL.Entities
{
    public class Log
    {
        public int Id { get; set; }
        public DateTime Dat { get; set; }
        public int Sum { get; set; }
        public int Op { get; set; }
        public int Card_Id { get; set; }
    }
}